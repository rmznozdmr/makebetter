import React from 'react'
import { NavigationComponent } from 'react-native-material-bottom-navigation'
import { TabNavigator } from 'react-navigation'
import { AppRegistry } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { WebView } from 'react-native';

class Diets extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Diets',
    tabBarIcon: () => (<Icon size={24} color="white" name="heartbeat" />)
  }

  render() {return (
                   <WebView
                          source={{uri: 'http://www.diyetvakti.net/search/3+ayda+20+kilo+verdiren+form%C3%BCl'}}
                          style={{marginTop: 20}}
                        />
                );  }
}

class Kurler extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Kurler',
    tabBarIcon: () => (<Icon size={24} color="white" name="arrows" />)
  }


  render() { return (
                   <WebView
                     userAgent={"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/61.0.3163.79 Chrome/61.0.3163.79 Safari/537.36"}
                     source={{uri: 'http://www.profsaracoglu.com/bitkisel-kurler'}}
                     style={{marginTop: 20}}
                   />
                 ); }
}

class Profile extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Profil',
    tabBarIcon: () => (<Icon size={24} color="red" name="user" />)
  }

  render() {  return (
                     <WebView
                       userAgent={"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/61.0.3163.79 Chrome/61.0.3163.79 Safari/537.36"}
                       source={{uri: 'https://web.whatsapp.com/'}}
                       style={{marginTop: 20}}
                     />
                  );}
}

const MakeBetter = TabNavigator({
  Diets: { screen: Diets },
  Kurler: { screen: Kurler },
  Profile: { screen: Profile }
}, {
  tabBarComponent: NavigationComponent,
  tabBarPosition: 'bottom',
  tabBarOptions: {
    bottomNavigationOptions: {
      labelColor: 'black',
      rippleColor: 'black',
      tabs: {
        Diets: {
          barBackgroundColor: '#37474F'
        },
        Kurler: {
          barBackgroundColor: '#00796B',
        },
        Profile: {
          barBackgroundColor: 'red',
          labelColor: '#434343', // like in the standalone version, this will override the already specified `labelColor` for this tab
          activeLabelColor: '#212121',
        }
      }
    }
  }
})

AppRegistry.registerComponent('MakeBetter', () => MakeBetter)