/**
 * Load the App component.
 *  (All the fun stuff happens in "/src/index.js")
 *
 * React Native Starter App
 *
 */

import { AppRegistry } from 'react-native';
import AppContainer from './src/';

AppRegistry.registerComponent('StarterKit', () => AppContainer);
